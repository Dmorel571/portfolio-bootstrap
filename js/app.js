const faders = document.querySelectorAll('.fade-in');
const appearOptions = {
    threshold: 1,
    rootMargin: "0px 0px -500px 0px"
};

const appearonScroll = new IntersectionObserver (function(entries,appearonScroll) {
    entries.forEach(entry => {
        if (!entry.isIntersecting){
            return;
        } else {
            entry.target.classList.add('appear');
            appearonScroll.unobserve(entry.target);
        }
    });
},
 appearOptions);

faders.forEach(fader => {
    appearonScroll.observe(fader);
});
